﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class col_trigger : MonoBehaviour {

    public GameObject Activator;

    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter(Collider target)
    {

        if (target.tag == "Player")
        {
            Activator.SetActive(true);
          //  StartCoroutine("WaitForSec");
        }
    }

    IEnumerator WaitForSec()
    {
        yield return new WaitForSeconds(5);
        Destroy(Activator);
        Destroy(gameObject);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
