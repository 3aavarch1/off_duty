using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShutterSwitch : MonoBehaviour {
	public GameObject shutter;
	Animator anim;
	public AudioClip clip;

	// Use this for initialization
	void Start () {
		anim=shutter.GetComponent<Animator>();
	}

	void OnMouseOver(){
		if (Input.GetKeyDown(KeyCode.F))
        {
        	FindObjectOfType<AudioManager>().Play("Button_01");
        	//Debug.Log("W!");
			anim.SetTrigger("isDown");
			AudioSource.PlayClipAtPoint(clip, new Vector3(26.9f, 8.43f, -108.4f));          
        }

	}	
	// Update is called once per frame
}
