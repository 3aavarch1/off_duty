﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue : MonoBehaviour {

	public GameObject Instructions;

	// Use this for initialization
	void Start () {
		
	}
	
	void OnTriggerEnter(Collider target){

		if(target.tag=="Player"){
			Instructions.SetActive(true);
			StartCoroutine("WaitForSec");
		}
	}

	IEnumerator WaitForSec(){
		yield return new WaitForSeconds(5);
		Destroy(Instructions);
		Destroy(gameObject);
	}
	// Update is called once per frame
	void Update () {
		
	}
}
