using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;

public class P_Melee_01 : vp_DamageHandler
{

	private Animator anim;
    private NavMeshAgent agent;
    public GameObject Player;

    public Transform[] patrolPoints;
    private int currentControlPointIndex = 0;

    public float DamagePoints = 2.0f;
    public float FollowDistance = 20.0f;
    public float AttackDistance = 3.0f;

    float rotSpeed = 5.0f;
    public float lookAtAngle=10.0f;


    protected override void Awake()
    {
        
        agent = this.GetComponent<NavMeshAgent>();
        anim = this.GetComponent<Animator>();
        MoveToNextPatrolPoint();
        anim.SetFloat("wOffset", Random.Range(0, 1));

    }

    void Start()
    {
        base.Awake();
    }


    void Update(){
       
    	if (agent.enabled){

    		float dist = Vector3.Distance(Player.transform.position, this.transform.position);

            bool attack = false;
   			bool patrol = false;
   			bool follow = (dist < FollowDistance);

        	if (follow)
            {
            	var LookingAtTarget=LookAtTarget();
                	if ((dist < AttackDistance) && LookingAtTarget){

                    	attack = true;

                	}

                agent.SetDestination(Player.transform.position);

            }

            patrol = !follow && !attack && patrolPoints.Length > 0;

            if ((!follow || attack) && !patrol)
                agent.SetDestination(transform.position);

        	// Patrolling between points if there are patrol points
        	if (patrol){
            	if (!agent.pathPending && agent.remainingDistance < 1.0f)
                	MoveToNextPatrolPoint();
        	}

        	anim.SetBool("Walk", follow || patrol);
        	anim.SetBool("Attack", attack);

    	}

	}

	public bool LookAtTarget()
    {
        Vector3 direction = Player.transform.position - this.transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, 
                                                Quaternion.LookRotation(direction), 
                                                Time.deltaTime*rotSpeed);
        
       	if(Vector3.Angle(this.transform.forward,direction) < lookAtAngle)
        {
        	//Debug.Log("lookingdirectly");
        	return true;
        }

        else{
        	//Debug.Log("notlookingdirectly");
            return false;
        }
    }

    void MoveToNextPatrolPoint(){
        if (patrolPoints.Length > 0)
        {
            agent.destination = patrolPoints[currentControlPointIndex].position;
            currentControlPointIndex++;
            currentControlPointIndex %= patrolPoints.Length;
        }
    }

    /// <summary>
    /// Character takes damage
    /// </summary>
    /// <param name="damageInfo"></param>
    public override void Damage(vp_DamageInfo damageInfo)
    {
        if (CurrentHealth > 0 )
        {

            AnimatorStateInfo si = anim.GetCurrentAnimatorStateInfo(0);
            if (!si.IsName("Attack"))
            {

                base.Damage(damageInfo);

                anim.Play("Hit", 0, 0.25f);     //0.25f
                FindObjectOfType<AudioManager>().Play("PlayerHit_02");
                agent.enabled = false;
            }
        }
    }

    /// <summary>
    /// Should be called from an animation event
    /// </summary>
    public void OnHitEnd()
    {
        agent.enabled = true;
    }

    /// <summary>
    /// Character dies
    /// </summary>
    public override void Die()
    {
        if (!enabled || !vp_Utility.IsActive(gameObject))
            return;

        if (m_Audio != null)
        {
            m_Audio.pitch = Time.timeScale;
            m_Audio.PlayOneShot(DeathSound);
        }

        agent.enabled = false;

        anim.SetBool("Walk", false);
        anim.SetBool("Attack", false);

        anim.SetTrigger("Die");

        Destroy(GetComponent<vp_SurfaceIdentifier>());
        //Destroy(this.gameObject); 
        //Destroy(gameObject);

    }

    public void EndAttack()
    {
        float dist = Vector3.Distance(Player.transform.position, this.transform.position);

        // TODO: Get rid of this magic number here: (perhaps add property)
        if(dist < 2.0f)
        {
            Player.SendMessage("Damage", DamagePoints, SendMessageOptions.DontRequireReceiver);
            FindObjectOfType<AudioManager>().Play("Punch_01");
        }
    }

}

