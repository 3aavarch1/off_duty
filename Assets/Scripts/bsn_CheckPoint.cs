﻿using UnityEngine;
using System.Collections;

public class bsn_CheckPoint : MonoBehaviour {
	public Transform spawnPoint = null;


	void OnTriggerEnter(Collider other) 
	{
// discover the existing spawn point, if any and destroy it!

		GameObject oldPoint = GameObject.Find("PlayerSpawnPoint(Clone)");
		if (oldPoint != null) Destroy(oldPoint);

// make the new spawn point right here where the trigger is located

		Instantiate(spawnPoint, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);

// discover the newly created spawn point

		GameObject thisPoint = GameObject.Find("PlayerSpawnPoint(Clone)");
		if (thisPoint != null) 
		{
// Gain access to the spawn point script and insure that we force
// the script to search for new waypoints when the player dies. Without this, the
// vp_SpawnPoint script will not correctly initialize when the player dies.
//It does seem to initialize properly on level changes and at application startup.

			vp_SpawnPoint thisList = thisPoint.GetComponentInChildren<vp_SpawnPoint>();
			thisList.ClearSpawnPoints();
		}


// now destroy the trigger object

		Destroy(this.gameObject);
	}

}