using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXAKBullet_Handler : vp_FXBullet {

    protected override void DoUFPSDamage()
    {
        // Must be a headshot if the BoxCollider is hit
        if(m_Hit.collider is SphereCollider)
        {
            Damage = 10.0f;
            //Debug.Log("head");
        } else if(m_Hit.collider is BoxCollider){

            Damage = 5.0f;
            // Debug.Log("body");
        }else if(m_Hit.collider is CapsuleCollider){

            Damage = 2.5f;
            // Debug.Log("limbs");
        }

        // Call base method
        base.DoUFPSDamage();
    }
}
