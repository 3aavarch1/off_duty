﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour {

    public GameObject Instructions;

    // Use this for initialization
    void Start () {
		
	}

    void OnMouseOver()
    {
        Instructions.SetActive(true);
        StartCoroutine("WaitForSec");
    }

    IEnumerator WaitForSec()
    {
        yield return new WaitForSeconds(5);
        Instructions.SetActive(false);

    }
    // Update is called once per frame
    void Update () {
		
	}
}
