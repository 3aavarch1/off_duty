﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
namespace red
{
    public class PauseDetect : MonoBehaviour
    {

        private GameObject FPS_Player;//UFPS Player - Grabbing this from the player
        private vp_FPPlayerEventHandler m_Player = null;//UFPS Event Handler for Player
        private vp_FPInput m_Input = null;//UFPS Input for Player

        public GameObject Pause;//Pause Menu


        void Awake()
        {
            FPS_Player = this.gameObject;
            m_Player = GameObject.FindObjectOfType(typeof(vp_FPPlayerEventHandler)) as vp_FPPlayerEventHandler;
            m_Input = GameObject.FindObjectOfType(typeof(vp_FPInput)) as vp_FPInput;
        }
        void Start()
        {
            Pause.SetActive(false);//Turns off the Pause Menu
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (m_Player.Pause.Get() == true)
                {
                    //m_Player.Pause.Set(false);
                    PauseMenu();
                    
                }
                else
                {
                    //m_Player.Pause.Set(true);
                    HideMenu();
                }

            }
        }


        public virtual void HideMenu()
        {
            //UNPause The Game Here
            Cursor.lockState = CursorLockMode.Locked;//Unity 5.2 lock the cursor
            Cursor.visible = false;//Unity 5.2 turn off the cursor
            m_Input.MouseCursorForced = false;//Change cursor back to normal in game mode

            Pause.SetActive(false);//Turn off Pause Menu

        }

        public virtual void PauseMenu()
        {
            Pause.SetActive(true);
            m_Input.MouseCursorForced = true;
            Cursor.visible = true;

        }

    }
}