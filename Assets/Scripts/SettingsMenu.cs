﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour {

	public AudioMixer audiomixer;

	// Use this for initialization
	public void setVolume(float volume){

		audiomixer.SetFloat("volume",volume);
	}

	public void setQuality(int qualityIndex){

		QualitySettings.SetQualityLevel(qualityIndex);
	}

	public void SetFullscreen(bool isFullscreen){

		Screen.fullScreen=isFullscreen;
	}
}
