using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class P_Shooter_01 : vp_DamageHandler
{
    private Animator _animator;
    private NavMeshAgent _navMeshAgent;
    public GameObject Player;
    public GameObject Gun;

    float rotSpeed = 5.0f;
    public float lookAtAngle=5.0f;
    public float AttackDistance = 10.0f;
    public float FollowDistance = 20.0f;

    [Range(0.0f, 1.0f)]
    public float HitAccuracy = 0.5f;
    public float DamagePoints = 2.0f;
    public AudioClip GunSound = null;
    public Transform[] patrolPoints;
    private int currentControlPointIndex = 0;

    public GameObject drop;//your Item
    public GameObject destroyGun;


    protected override void Awake()
    {
       
        _navMeshAgent = this.GetComponent<NavMeshAgent>();
        _animator = this.GetComponent<Animator>();
        MoveToNextPatrolPoint();
        _animator.SetFloat("wOffset", Random.Range(0, 1));

    }

    void Start(){
        base.Awake();
    }

    // Update is called once per frame
    void Update(){

        if (_navMeshAgent.enabled){
            
            float dist = Vector3.Distance(Player.transform.position, this.transform.position);

            bool shoot = false;
            bool patrol = false;
            bool follow = (dist < FollowDistance);

            if (follow)
            {
                if (dist < AttackDistance){		
                    var SeePlayer=noWall();
                    var LookingAtTarget=LookAtTarget();
                        if (SeePlayer && LookingAtTarget){
                            shoot = true;
                    }
                }

                _navMeshAgent.SetDestination(Player.transform.position);
            }

            patrol = !follow && !shoot && patrolPoints.Length > 0;

            if ((!follow || shoot) && !patrol)
                _navMeshAgent.SetDestination(transform.position);

            // Patrolling between points if there are patrol points
            if (patrol)
            {
                if (!_navMeshAgent.pathPending && 
                    _navMeshAgent.remainingDistance < 0.5f)
                    MoveToNextPatrolPoint();
            }

            _animator.SetBool("Shoot", shoot);
            _animator.SetBool("Run", follow || patrol);

            // TODO: Add a walk animation for patrol

        }
    }

    bool noWall()
    {
        Vector3 distance = Player.transform.position - Gun.transform.position; 
        
        RaycastHit hit;
        bool seeWall = false;

        Debug.DrawRay(Gun.transform.position,distance, Color.red);

        if (Physics.Raycast(Gun.transform.position, distance, out hit))
        {
            if(hit.collider.gameObject.tag == "wall")
            {
                seeWall = true;
            }
        }

        if(!seeWall){
        	Debug.Log("no obstacle");
        	return true;
        }

        else{
        	Debug.Log("obstacle");
            return false;
        }
    }

    public bool LookAtTarget()
    {
        Vector3 direction = Player.transform.position - this.transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, 
                                                Quaternion.LookRotation(direction), 
                                                Time.deltaTime*rotSpeed);
        
       	if(Vector3.Angle(this.transform.forward,direction) < lookAtAngle)
        {
        	//Debug.Log("lookingdirectly");
        	return true;
        }

        else{
        	//Debug.Log("notlookingdirectly");
            return false;
        }
    }


    void MoveToNextPatrolPoint()
    {
        if (patrolPoints.Length > 0)
        {
            _navMeshAgent.destination = patrolPoints[currentControlPointIndex].position;

            currentControlPointIndex++;
            currentControlPointIndex %= patrolPoints.Length;
        }
    }

    public void ShootEvent()
    {
        if (m_Audio != null)
        {
            m_Audio.PlayOneShot(GunSound);
        }

        float random = Random.Range(0.0f, 1.0f);

        // The higher the accuracy is, the more likely the player will be hit
        bool isHit = random > 1.0f - HitAccuracy;

        if (isHit)
        {
            Player.SendMessage("Damage", DamagePoints, 
                SendMessageOptions.DontRequireReceiver);
        }
    }

    // <summary>
    /// Character takes damage
    /// </summary>
    /// <param name="damageInfo"></param>
    public override void Damage(vp_DamageInfo damageInfo)
    {
        if (CurrentHealth > 0 )
        {

            AnimatorStateInfo si = _animator.GetCurrentAnimatorStateInfo(0);
            if (!si.IsName("Shoot"))
            {

                base.Damage(damageInfo);

                _animator.Play("Hit", 0, 0.25f);

                _navMeshAgent.enabled = false;
            }
        }
    }

    /// <summary>
    /// Should be called from an animation event
    /// </summary>
    public void OnHitEnd()
    {
        _navMeshAgent.enabled = true;
    }

    public override void Die()
    {
        if (!enabled || !vp_Utility.IsActive(gameObject))
            return;

        if (m_Audio != null)
        {
            m_Audio.pitch = Time.timeScale;
            m_Audio.PlayOneShot(DeathSound);
        }

        _navMeshAgent.enabled = false;

        _animator.SetBool("Run", false);
        _animator.SetBool("Shoot", false);

        _animator.SetTrigger("Die");
        Instantiate(drop, transform.position, drop.transform.rotation); //your dropped Item

        Destroy(GetComponent<vp_SurfaceIdentifier>());
        Destroy(destroyGun);

    }
}
