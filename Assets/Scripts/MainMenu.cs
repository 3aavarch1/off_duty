using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MainMenu : MonoBehaviour {

	public GameObject loadingScreen;
	public Slider slider;
	public AudioMixer audiomixer;
	private vp_FPInput m_Input;

	void awake(){
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = true;
		m_Input = GameObject.FindObjectOfType(typeof(vp_FPInput)) as vp_FPInput;
	}

	void start(){
		m_Input.MouseCursorForced = true;
		vp_Utility.LockCursor = false;
	}


	public void playGame(){
		StartCoroutine(LoadAsynchronously());
		//SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	IEnumerator LoadAsynchronously(){

		AsyncOperation operation=SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
		loadingScreen.SetActive(true);

		while(!operation.isDone){
			float progress=Mathf.Clamp01(operation.progress/0.9f);
			slider.value=progress;
			yield return null;
		}
	}

	public void setVolume(float volume){

		audiomixer.SetFloat("volume",volume);
	}

	public void setQuality(int qualityIndex){

		QualitySettings.SetQualityLevel(qualityIndex);
	}

	public void SetFullscreen(bool isFullscreen){

		Screen.fullScreen=isFullscreen;
	}

	public void QuitGame(){

		Application.Quit();
		//Debug.Log("quit");
	}
}
