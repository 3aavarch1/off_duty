﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIctivate : MonoBehaviour {

    public GameObject[] AI;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            foreach(GameObject enemy in AI){
                enemy.SetActive(true);
            }
        }
    }
}
