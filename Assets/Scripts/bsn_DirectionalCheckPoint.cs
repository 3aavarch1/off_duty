﻿using UnityEngine;
using System.Collections;

public class bsn_DirectionalCheckPoint : MonoBehaviour {
    public Transform spawnPoint = null;
    public float direction = 0f;

    void OnTriggerEnter(Collider other)
    {
        // discover the existing DIRECTIONAL spawn point, if any, and DEACTIVATE it!

        GameObject oldPoint = GameObject.Find("PlayerSpawnPointDirectional(Clone)");
        if (oldPoint != null) oldPoint.SetActive(false);

        // make the new spawn point right here where the trigger is located

        Instantiate(spawnPoint, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);


        // discover the newly created DIRECTIONAL spawn point

        GameObject thisPoint = GameObject.Find("PlayerSpawnPointDirectional(Clone)");
        if (thisPoint != null)
        {


            //set the rotation of the DIRECTIONAL spawnpoint

            thisPoint.transform.rotation = Quaternion.Euler(new Vector3(0, direction, 0));

            // Gain access to the spawn point script and insure that we force
            // the script to search for new waypoints when the player dies. Without this, the
            // vp_SpawnPoint script will not correctly initialize when the player dies.
            //It does seem to initialize properly on level changes and at application startup.

            vp_SpawnPoint thisList = thisPoint.GetComponentInChildren<vp_SpawnPoint>();
            thisList.ClearSpawnPoints();
        }


        // now destroy the trigger object SO THAT IT DOES NOT CREATE THIS SPAWN POINT AGAIN

        Destroy(this.gameObject);
    }

}