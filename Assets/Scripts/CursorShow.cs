﻿using UnityEngine;
using System.Collections;

public class CursorShow : MonoBehaviour 
{
	
	private vp_FPCamera m_Camera;
	private vp_FPPlayerEventHandler m_Player;
	private vp_FPInput m_Input;
	private vp_FPController m_Controller;

	private bool doNoCursor = false;
	private bool doYesCursor = false;



	void Start () 
	{
		m_Player = GameObject.FindObjectOfType(typeof(vp_FPPlayerEventHandler)) as vp_FPPlayerEventHandler;
		m_Camera = GameObject.FindObjectOfType(typeof(vp_FPCamera)) as vp_FPCamera;
		m_Input = GameObject.FindObjectOfType(typeof(vp_FPInput)) as vp_FPInput;
		m_Controller = GameObject.FindObjectOfType(typeof(vp_FPController)) as vp_FPController;
	}
	
	void Update () 
	{
		// for cases where you don't want to show cursor (e.g. chat ui)
		if (Input.GetKeyDown(KeyCode.Z))
		{
			if (doYesCursor) return; // no chat if UI active
			doNoCursor = !doNoCursor; // toggle chat on/off
			LockInputAndCursor(doNoCursor);
		} 

		// for using all sorts of gui
		if (Input.GetKeyDown(KeyCode.X))
		{
			if(doNoCursor) return; // no UI if chat active
			doYesCursor = !doYesCursor; // toggle UI on/off
			LockInput(doYesCursor);
		} 
	}

	// for cases where you don't want to show cursor (e.g. chat ui)
	public void LockInputAndCursor(bool locked)
	{
		m_Controller.Stop();
		m_Player.Attack.TryStop(); // just in case
		m_Player.InputAllowGameplay.Set(!locked);
		m_Camera.SetState("Freeze", locked);
		vp_Utility.LockCursor = true;
		ShowSomethingNoCursor(locked);
	}

	// for using all sorts of gui
	public void LockInput(bool locked)
	{
		m_Controller.Stop();
		m_Player.Attack.TryStop(); // just in case
		m_Player.InputAllowGameplay.Set(!locked);
		m_Camera.SetState("Freeze", locked);
		m_Input.MouseCursorForced = locked;
		vp_Utility.LockCursor = !locked;
		ShowSomethingYesCursor(locked);
	}

	void ShowSomethingNoCursor(bool showThis)
	{
		if (showThis)
		{
			Debug.Log("Player WSAD movement stopped. Camera movement OK. No cursor");
			// show something that doesn't want a cursor
		}
		else
		{
			Debug.Log("Player WSAD movement allowed. Camera movement OK. No cursor");
			// close something that didn't want a cursor
		}
	}

	void ShowSomethingYesCursor(bool showThis)
	{
		if (showThis)
		{
			Debug.Log("Player WSAD movement stopped. Camera movement stopped. Yes cursor");
			// show something that does want a cursor
		}
		else
		{
			Debug.Log("Player WSAD movement allowed. Camera movement OK. No cursor");
			// close something that did want a cursor
		}
	}
}
