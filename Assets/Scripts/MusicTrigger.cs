﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTrigger : MonoBehaviour {

    public AudioClip newTrack;
    private MusicManager theMM;

    void Start()
    {
        theMM = FindObjectOfType<MusicManager>();
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            theMM.changeBGM(newTrack);

        }
    }
}
