﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShutterDown : MonoBehaviour {
	public GameObject shutter;
	Animator anim;
	public AudioClip clip;

	// Use this for initialization
	void Start () {
		anim=shutter.GetComponent<Animator>();
	}
	
	private void OnTriggerEnter(Collider other){
		if(other.gameObject.tag=="Player"){
			anim.SetTrigger("isDown");
			AudioSource.PlayClipAtPoint(clip, new Vector3(-31f, 5.75f, -106.17f));
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
