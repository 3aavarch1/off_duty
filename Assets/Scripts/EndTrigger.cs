using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EndTrigger : MonoBehaviour {

	public GameManager gameManager;
    public GameObject endGameCutscene;
    public AudioClip clip;

    public GameObject AI_01;
    public GameObject AI_02;
    public GameObject AI_03;
    public GameObject AI_04;
    public GameObject AI_05;
    public GameObject AI_06;
    public GameObject AI_07;
    public GameObject AI_08;



    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            AI_01.SetActive(false);
            AI_02.SetActive(false);
            AI_03.SetActive(false);
            AI_04.SetActive(false);
            AI_05.SetActive(false);
            AI_06.SetActive(false);
            AI_07.SetActive(false);
            AI_08.SetActive(false);

            AudioSource.PlayClipAtPoint(clip, new Vector3(56.25f, 8.41f, -108.5f));
            endGameCutscene.SetActive(true);
            gameManager.completeLevel();
        }
    }
}
