﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    public AudioSource backgroundMusic;


    public void changeBGM(AudioClip music)
    {
        if (backgroundMusic.clip.name == music.name)
            return;

        backgroundMusic.Stop();
        backgroundMusic.clip = music;
        backgroundMusic.Play();

    }


}
