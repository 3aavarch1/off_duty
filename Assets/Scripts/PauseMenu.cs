﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class PauseMenu : MonoBehaviour {

	public static bool GameIsPaused=false;
	public GameObject pauseMenuUI;
	public GameObject settingsMenuUI;
	public AudioMixer audiomixer;
	//public GameObject Player;


	private vp_FPCamera m_Camera;
	private vp_FPPlayerEventHandler m_Player;
	private vp_FPInput m_Input;
	private vp_FPController m_Controller;

	private bool doNoCursor = false;
	private bool doYesCursor = false;


    void Start () 
	{
		m_Player = GameObject.FindObjectOfType(typeof(vp_FPPlayerEventHandler)) as vp_FPPlayerEventHandler;
		m_Camera = GameObject.FindObjectOfType(typeof(vp_FPCamera)) as vp_FPCamera;
		m_Input = GameObject.FindObjectOfType(typeof(vp_FPInput)) as vp_FPInput;
		m_Controller = GameObject.FindObjectOfType(typeof(vp_FPController)) as vp_FPController;     
	}


	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			if(doNoCursor) return; // no UI if chat active
			doYesCursor = !doYesCursor; // toggle UI on/off
			LockInput(doYesCursor);

			if(GameIsPaused){
				Resume();
			}else{
				Pause();
			}
		}
	}

	public void Resume(){
		pauseMenuUI.SetActive(false);
		settingsMenuUI.SetActive(false);
		Time.timeScale=1f;
		GameIsPaused=false;
		doYesCursor = !doYesCursor;
		LockInput(doYesCursor);

	}

	void Pause(){
		pauseMenuUI.SetActive(true);
		//settingsMenuUI.SetActive(true);
		Time.timeScale=0f;
		GameIsPaused=true;
		doYesCursor = !doYesCursor;

	}

	// for using all sorts of gui
	public void LockInput(bool locked)
	{
		m_Controller.Stop();
		m_Player.Attack.TryStop(); // just in case
		m_Player.InputAllowGameplay.Set(!locked);
		m_Camera.SetState("Freeze", locked);
		m_Input.MouseCursorForced = locked;
		vp_Utility.LockCursor = !locked;

	}

	public void mainMenu(){
		SceneManager.LoadScene("Main_Menu");
		
	}

	public void setVolume(float volume){

		audiomixer.SetFloat("volume",volume);
	}

	public void setQuality(int qualityIndex){

		QualitySettings.SetQualityLevel(qualityIndex);
	}

	public void SetFullscreen(bool isFullscreen){

		Screen.fullScreen=isFullscreen;
	}

	public void quitGame(){
		Application.Quit();
	}
}
