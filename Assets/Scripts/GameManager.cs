﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public GameObject CompleteLevelUI;
	public GameObject Player;

	//void Awake(){
		//StartCoroutine(ControlAfterTime(32));
	//}

	public void completeLevel(){

		StartCoroutine(ExecuteAfterTime(35));
		
	}

	IEnumerator ExecuteAfterTime(float time)
 	{
    	yield return new WaitForSeconds(time);
 		SceneManager.LoadScene("Main_Menu");
 		//Player.SetActive(false);
 		Cursor.visible=true;
		Cursor.lockState = CursorLockMode.None;
		//vp_Utility.LockCursor = true;
     // Code to execute after the delay
    }

    IEnumerator ControlAfterTime(float time)
 	{
    	yield return new WaitForSeconds(time);
    	Player.SetActive(true);
    }
	
}
