using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXPistolBullet_Handler : vp_FXBullet {

    protected override void DoUFPSDamage()
    {
        // Must be a headshot if the BoxCollider is hit
        if(m_Hit.collider is SphereCollider)
        {
            Damage = 5.0f;
           //Debug.Log("head");
        } else if(m_Hit.collider is BoxCollider){

            Damage = 2.5f;
            // Debug.Log("body");
        }else if(m_Hit.collider is CapsuleCollider){

            Damage = 2.0f;
             //Debug.Log("limbs");
        }

        // Call base method
        base.DoUFPSDamage();
    }
}
