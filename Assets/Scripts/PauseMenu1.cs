﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class PauseMenu1 : MonoBehaviour {

	public static bool GameIsPaused=false;
	public GameObject pauseMenuUI;
	public GameObject settingsMenuUI;
	public AudioMixer audiomixer;


	private vp_FPCamera m_Camera;
	private vp_FPPlayerEventHandler m_Player;
	private vp_FPInput m_Input;
	private vp_FPController m_Controller;

    void Awake () 
	{
		m_Player = GameObject.FindObjectOfType(typeof(vp_FPPlayerEventHandler)) as vp_FPPlayerEventHandler;
		m_Camera = GameObject.FindObjectOfType(typeof(vp_FPCamera)) as vp_FPCamera;
		m_Input = GameObject.FindObjectOfType(typeof(vp_FPInput)) as vp_FPInput;
		m_Controller = GameObject.FindObjectOfType(typeof(vp_FPController)) as vp_FPController;     
	}


	public void mainMenu(){
		SceneManager.LoadScene("Main_Menu");
		
	}

	public void setVolume(float volume){

		audiomixer.SetFloat("volume",volume);
	}

	public void setQuality(int qualityIndex){

		QualitySettings.SetQualityLevel(qualityIndex);
	}

	public void SetFullscreen(bool isFullscreen){

		Screen.fullScreen=isFullscreen;
	}

	public void quitGame(){
		Application.Quit();
	}
}
